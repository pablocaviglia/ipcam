package org.pol.ipcamera.videotransform.config;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;

public class ConfigReader {
	
	public static String DIAS_PROCESAMIENTO;
	
	public static String SYSTEM_FOLDER;
	public static final String CONFIG_FILE = "system.cfg";

	private static HashMap<String, String> configValues = new HashMap<String, String>();
	
	public static void init(String systemFolder) {
		
		try {
			SYSTEM_FOLDER = systemFolder;
			BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(new File(SYSTEM_FOLDER, CONFIG_FILE))));
			String line = null;
			try {
				while((line=reader.readLine()) != null) {
					if(!line.startsWith("#") && !line.trim().equals("")) {
						String[] splitted = line.split("=");
						String key = splitted[0].trim();
						String value = splitted[1].trim();
						configValues.put(key, value);
					}
				}
			} 
			catch (IOException e) {
				e.printStackTrace();
				System.exit(0);
			}
		} 
		catch (FileNotFoundException e) {
			System.out.println("--------------------------------- ERROR ---------------------------------");
			System.out.println("No se puede acceder al archivo de configuracion.");
			System.out.println("El mismo debe de estar localizado en:");
			System.out.println(SYSTEM_FOLDER + CONFIG_FILE);
			System.out.println("-------------------------------------------------------------------------");
			System.exit(0);
		}
	}
	
	public static String obtenerCarpetaVideos() {
		return configValues.get("carpeta_videos");
	}
	
	public static String obtenerComandoMencoder() {
		return configValues.get("comando_mencoder");
	}

	public static String obtenerZonaHoraria() {
		return configValues.get("zona_horaria");
	}

	public static String obtenerDiasProcesamiento() {
		if(DIAS_PROCESAMIENTO != null) {
			return DIAS_PROCESAMIENTO;
		}
		else {
			return configValues.get("dias_procesamiento");
		}
	}
}