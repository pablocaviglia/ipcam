package org.pol.ipcamera.videotransform.util;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;

public class StreamHandlerThread extends Thread {
	
	private InputStream inputStream;
	
	public StreamHandlerThread(InputStream inputStream) {
		this.inputStream = inputStream;
	}
	
	@Override
	public void run() {
		
		try {
			
			BufferedInputStream bis = new BufferedInputStream(inputStream);
			int i = -1;
			byte[] buffer = new byte[1024];
			while((i = bis.read(buffer)) != -1) {
				//do nothing
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			try {
				inputStream.close();
			} 
			catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
