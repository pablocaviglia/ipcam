package org.pol.ipcamera.videotransform;

import java.text.SimpleDateFormat;
import java.util.TimeZone;

import org.pol.ipcamera.videotransform.config.ConfigReader;

public class VideoTransformThread extends Thread {
	
	//formateo de fecha para solamente
	//obtener las horas y los minutos
	private SimpleDateFormat horaMinutosFormat = new SimpleDateFormat("HHmm");

	//modulo transformador de videos
	private VideoTransformExecutor videoTransformExecutor;
	
	public VideoTransformThread() {

		horaMinutosFormat.setTimeZone(TimeZone.getTimeZone("GMT" + ConfigReader.obtenerZonaHoraria()));

		//creamos la instancia encargada 
		//de procesar los videos 
		videoTransformExecutor = new VideoTransformExecutor();
		
		//comenzamos el hilo
		start();
		
	}
	
	@Override
	public void run() {
		
		while(true) {
			
			try {
				videoTransformExecutor.procesarVideos();	
			}
			catch (Exception e) {
				e.printStackTrace();
			}
			
			//dormimos el hilo por 
			//un periodo determinado
			try {Thread.sleep(10000);} catch (InterruptedException e) {e.printStackTrace();}
		}
	}
	
	
	private static String obtenerParametroEntrada(String nombreArg, String[] args) {
		
		if(args != null && args.length > 0) {
			for(String arg : args) {
				if(arg.startsWith(nombreArg+"=")) {
					return arg.replaceAll(nombreArg + "=", "").trim();
				}
			}
		}
		
		return null;
	}
	
	public static void main(String[] args) {
		try {
			
			String config = obtenerParametroEntrada("config", args);
			String diasProcesamiento = obtenerParametroEntrada("dias_procesamiento", args);
			
			if(args == null || args.length == 0) {
				System.out.println("--------------- VIDEO TRANSFORMER PARAMS ---------------");
				System.out.println("config=/home/ubuntu/videotransform/\t\tCarpeta donde se localiza el archivo de configuracion (* OBLIGATORIO)");
				System.out.println("dias_procesamiento=3\t\t\tCantidad de dias previos al de hoy que se van a procesar videos (NO OBLIGATORIO)");
			}
			else {
				
				if(config == null || (config != null && config.trim().equals(""))) {
					System.out.println("--------------------------------- ERROR ---------------------------------");
					System.out.println("Debe de especificar la carpeta donde se encuentra el archivo");
					System.out.println("de configuracion como parametro de este programa.");
					System.out.println("-------------------------------------------------------------------------");
				}
				else {
					
					if(diasProcesamiento != null && !diasProcesamiento.trim().equals("")) {
						ConfigReader.DIAS_PROCESAMIENTO = diasProcesamiento;
					}
					
					//inicializamos el sistema
					ConfigReader.init(config);
					new VideoTransformThread();
					
				}
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}