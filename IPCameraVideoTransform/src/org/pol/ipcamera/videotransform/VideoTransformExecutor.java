package org.pol.ipcamera.videotransform;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.pol.ipcamera.videotransform.config.ConfigReader;
import org.pol.ipcamera.videotransform.util.FilenameComparator;
import org.pol.ipcamera.videotransform.util.StreamHandlerThread;

public class VideoTransformExecutor {
	
	//formateo de fecha para solamente
	//obtener el ano, mes y dia
	private SimpleDateFormat anoMesDiaFormat = new SimpleDateFormat("yyyyMMdd");
	
	public VideoTransformExecutor() {
		anoMesDiaFormat.setTimeZone(TimeZone.getTimeZone("GMT" + ConfigReader.obtenerZonaHoraria()));
	}
	
	/**
	 * Procesa los videos que hayan sido grabados
	 * por todas las camaaras de todos los clientes
	 * la cantidad de dias hacia atras indicado en
	 * el archivo de configuracion
	 * @throws IOException 
	 */
	public void procesarVideos() throws IOException {
		
		//devuelve las carpetas 
		//de todos los clientes
	    List<File> carpetasClientes = obtenerCarpetasClientes();
		
	    //recorremos la lista de 
	    //carpetas de los clientes
	    for(File carpetaCliente : carpetasClientes) {
	    	
	    	//cliente
	    	String cliente = carpetaCliente.getName();
	    	
	    	//obtenemos la cantidad de dias hacia atras 
	    	//desde hoy que debemos de comenzar a procesar
	    	//videos
	    	int cantidadDiasProcesamiento = Integer.parseInt(ConfigReader.obtenerDiasProcesamiento().trim());
	    	
	    	//procesamos los videos de cada uno
	    	//de los dias previos configurados
	    	for(int i=cantidadDiasProcesamiento; i>=0; i--) {
	    		
				//obtenemos la fecha del dia de ayer
				Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
				calendar.setTime(new Date());
	    		calendar.set(Calendar.DAY_OF_MONTH, calendar.get(Calendar.DAY_OF_MONTH) - i);
				Date fechaPreviaDate = calendar.getTime();
				
				//obtiene la lista de carpetas de las camaras
				//que tuvieron grabaciones para la fecha dada
				List<File> carpetasCamaras = obtenerCarpetasCamaras(cliente, fechaPreviaDate);
				
				//si hay camaras disponibles 
				//para el cliente
				if(carpetasCamaras.size() > 0) {
					//recorremos la lista de camaras para analizar
					//el contenido grabado por cada una 
					for(File carpetaCamaraActual : carpetasCamaras) {
						//procesamos la carpeta de video
						procesarCarpetaVideos(carpetaCamaraActual);
					}
				}
	    	}
	    }
	}
	
	/**
	 * Procesa la carpeta de videos, primero uniendo todos
	 * los videos en un solo archivo, y luego uniendo los
	 * archivos descriptores en uno solo.
	 * @param carpetaVideos
	 * @throws IOException 
	 */
	private void procesarCarpetaVideos(File carpetaVideos) throws IOException {
		
		//obtenemos la lista de videos que su nombre
		//finalice con el patron '_part.asf'
		File[] videoPartesArray = carpetaVideos.listFiles(new FilenameFilter() {
			public boolean accept(File arg0, String arg1) {
				if (arg1.endsWith("_WOIndex.asf")) {
					return true;
				}
				return false;
			}
		});
		
		//convertimos a lista para ordenar
		List<File> videoPartesList = Arrays.asList(videoPartesArray);
		
		//ordenamos la lista
		Collections.sort(videoPartesList, new FilenameComparator());

		//realizamos la indexacion
		//de cada uno de los videos
		procesarVideos(videoPartesList);
		
	}
	
	/**
	 * Indexa cada uno de los videos recibidos
	 * 
	 * @param videos
	 */
	private void procesarVideos(List<File> videos) {
	
		
		for(File videoNoIndexadoActual : videos) {
			
			System.out.println("(INFO) Reindexando video: " + videoNoIndexadoActual.getAbsolutePath());
			
			//el archivo de salida se encuentra en el
			//mismo path que el video original y al
			//nombre se le remueve el tag '_WOIndex'
			File archivoSalida = new File(videoNoIndexadoActual.getAbsolutePath().replaceAll("_WOIndex", ""));
			
			//comando mencoder para 
			//indexar el video
			String mencoderStr = ConfigReader.obtenerComandoMencoder() + " -oac copy -ovc copy -idx -o " + archivoSalida.getAbsolutePath() + " " + videoNoIndexadoActual.getAbsolutePath();
			
			try {
				
				//ejecutamos el proceso
				Process process = Runtime.getRuntime().exec(mencoderStr);
				
				// handle process' stdout stream
				Thread out = new StreamHandlerThread(process.getInputStream());
				out.start();

				// handle process' stderr stream
				Thread err = new StreamHandlerThread(process.getErrorStream());
				err.start();

 				int exitVal = process.waitFor(); // InterruptedException

				out.join();
				err.join();
				
				//borramos el video ya indexado
				videoNoIndexadoActual.delete();
				
			} 
			catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Obtiene las carpetas correspondientes
	 * a todos los clientes que hayan disponibles
	 * 
	 * @param anoMesDia
	 * @return
	 */
	private List<File> obtenerCarpetasClientes() {
		
		//obtenemos la carpeta raiz donde se
		//localizan las carpetas de los clientes
		String carpetaRaizStr = ConfigReader.obtenerCarpetaVideos();

		//creamos la ruta completa a la
		//carpeta de los clientes
		File carpetaClientes = new File(carpetaRaizStr + "/");
		
		//lista de carpetas de los clientes
		List<File> carpetasClientes = Arrays.asList(carpetaClientes.listFiles());
		
		return carpetasClientes;
	}

	/**
	 * Dado un cliente y una fecha, devuelve todas las carpetas correspondientes
	 * a cada una de las camaras que contiene grabaciones para ese dia
	 * 
	 * @param anoMesDia
	 * @return
	 */
	private List<File> obtenerCarpetasCamaras(String cliente, Date fecha) {
		
		//lista de carpetas de las camaras
		List<File> carpetasCamaras = new ArrayList<File>();
		
		//obtenemos formateada el ano mes y dia 
		//de la fecha recibida por parametro
		String fechaFormattedStr = anoMesDiaFormat.format(fecha);
		
		//obtenemos la carpeta raiz donde se
		//localizan las carpetas con las fechas
		String carpetaVideosStr = ConfigReader.obtenerCarpetaVideos();
		
		//creamos la ruta completa a la
		//carpeta de videos y la fecha
		File carpetaFechas = new File(carpetaVideosStr + "/" + cliente + "/" + fechaFormattedStr);
		
		if(carpetaFechas.exists()) {
			
			//carpetas de camaras como arreglo
			String[] camarasArray = carpetaFechas.list();
			
			//creamos la referencia a las carpetas
			//reales de cada una de las camaras
			for(String camaraActual : camarasArray) {
				carpetasCamaras.add(new File(carpetaFechas.getAbsolutePath(), camaraActual));
			}
		}
		
		return carpetasCamaras;
	}
}