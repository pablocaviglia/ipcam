Very old Java project, at the moment designed to be used from Raspberry PI devices, to control surveillance cameras.
It makes use of the camera motion detector as a trigger to start recording.
There's an uploaded queue to save the recorded videos to the amazon cloud.
It also makes use amazon cloud video conversion services to transform the uploaded videos to standard streaming formats.