package org.pol.ipcamera.util;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.vfs2.FileContent;
import org.apache.commons.vfs2.FileSystemException;

public class ProgressInputStream extends InputStream {

    private final long size;
    private long progress;
    private final InputStream inputStream;
    private final String name;
    private boolean closed = false;

    public ProgressInputStream(String name, InputStream inputStream, long size) {
        this.size = size;
        this.inputStream = inputStream;
        this.name = name;
    }

    public ProgressInputStream(String name, FileContent content)
    throws FileSystemException {
        this.size = content.getSize();
        this.name = name;
        this.inputStream = content.getInputStream();
    }

    @Override
    public void close() throws IOException {
        super.close();
        if (closed) throw new IOException("already closed");
        closed = true;
    }

    @Override
    public int read() throws IOException {
        int count = inputStream.read();
        if (count > 0)
            progress += count;
//        updateTransfer(name, progress, size, count);
        return count;
    }
    @Override
    public int read(byte[] b, int off, int len) throws IOException {
        int count = inputStream.read(b, off, len);
        if (count > 0) {
            progress += count;
        }
//        updateTransfer(name, progress, size, count);
        return count;
    }
    
    private long progressStatus;
    private void updateTransfer(String name, long progress, long size, int count) {
    	progressStatus += count;
    	//si el incremento es mayor a 100kb
    	//mostramos mensaje
    	if(progressStatus >= (1024 * 100)) {
    		progressStatus = 0;
    		System.out.println("(INFO) Progeso de subida de archivo '" + name + "': " + (progress / 1024) + "kb / " + (size / 1024) + "kb");
    	}
    	
    	//final
    	if(progress >= size) {
    		System.out.println("(INFO) Progeso de subida de archivo '" + name + "': " + (progress / 1024) + "kb / " + (progress / 1024) + "kb");
    	}
    }
}