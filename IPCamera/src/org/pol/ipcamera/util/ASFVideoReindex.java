package org.pol.ipcamera.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

import org.pol.ipcamera.util.listener.VideoReindexerListener;

public class ASFVideoReindex extends Thread {
	
	private File input;
	private File output;
	private VideoReindexerListener listener;
	
	public ASFVideoReindex(File input, File output, VideoReindexerListener listener) {
		this.input = input;
		this.output = output;
		this.listener = listener;
		start();
	}
	
	@Override
	public void run() {
		
		try {
			
			System.out.println("STARTING VIDEO INDEXING...");
			
			String str = "mencoder " + input.getAbsolutePath() + " -o " + output.getAbsolutePath() + " -forceidx -ovc copy -oac copy";
			Process process = Runtime.getRuntime().exec(str);
			BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
			String tmp = null;
			while((tmp = reader.readLine()) != null) {
//				System.out.println(tmp);
			}
			reader.close();
			
			reader = new BufferedReader(new InputStreamReader(process.getErrorStream()));
			while((tmp = reader.readLine()) != null) {
//				System.out.println(tmp);
			}
			reader.close();
			
			if(listener != null) {
				listener.videoReindexed(input, output);
			}
		} 
		catch (IOException e) {
			e.printStackTrace();
		}
	}
}