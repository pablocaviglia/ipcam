package org.pol.ipcamera.util;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Date;

import org.pol.ipcamera.config.ConfigReader;

public class FileLogger {
	
	public synchronized static void logError(String msg) {
		
		try {
			msg = (new Date() + " --- " + msg + "\n");
			FileOutputStream fos = new FileOutputStream(new File(ConfigReader.SYSTEM_FOLDER, ConfigReader.obtenerLogErrorFile()), true);
			fos.write(msg.getBytes());
			fos.flush();
			fos.close();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	public synchronized static void log(String msg) {
		
		try {
			msg = (new Date() + " --- " + msg + "\n");
			FileOutputStream fos = new FileOutputStream(new File(ConfigReader.SYSTEM_FOLDER, ConfigReader.obtenerLogFile()), true);
			fos.write(msg.getBytes());
			fos.flush();
			fos.close();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}