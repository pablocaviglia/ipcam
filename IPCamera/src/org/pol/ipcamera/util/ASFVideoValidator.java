package org.pol.ipcamera.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

public class ASFVideoValidator extends Thread {
	
	public static boolean validate(File videoFile) {
		
		try {
			
			if(!videoFile.exists()) {
				return false;
			}
			
			String str = "avconv -i " + videoFile.getAbsolutePath();
			Process process = Runtime.getRuntime().exec(str);
			BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
			String tmp = null;
			while((tmp = reader.readLine()) != null) {
//				System.out.println(tmp);
				if(tmp.indexOf("1k tbr") != -1) {
					return false;
				}
			}
			reader.close();
			
			reader = new BufferedReader(new InputStreamReader(process.getErrorStream()));
			while((tmp = reader.readLine()) != null) {
//				System.out.println(tmp);
				if(tmp.indexOf("1k tbr") != -1) {
					return false;
				}
			}
			reader.close();
		} 
		catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	public static void main(String[] args) {
		
		System.out.println(ASFVideoValidator.validate(new File(args[0])));
	}
}