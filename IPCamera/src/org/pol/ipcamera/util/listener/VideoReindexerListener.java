package org.pol.ipcamera.util.listener;

import java.io.File;

public interface VideoReindexerListener {

	public void videoReindexed(File input, File output);
	
}
