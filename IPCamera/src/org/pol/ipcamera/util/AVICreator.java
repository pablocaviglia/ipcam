package org.pol.ipcamera.util;

import static org.monte.media.FormatKeys.EncodingKey;
import static org.monte.media.FormatKeys.FrameRateKey;
import static org.monte.media.FormatKeys.MediaTypeKey;
import static org.monte.media.VideoFormatKeys.DepthKey;
import static org.monte.media.VideoFormatKeys.ENCODING_AVI_MJPG;
import static org.monte.media.VideoFormatKeys.HeightKey;
import static org.monte.media.VideoFormatKeys.QualityKey;
import static org.monte.media.VideoFormatKeys.WidthKey;

import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import org.monte.media.Format;
import org.monte.media.FormatKeys.MediaType;
import org.monte.media.avi.AVIWriter;
import org.monte.media.math.Rational;

public class AVICreator {

	public static void main(String[] args) {
		
		try {
			
			File aviSalida = new File("c:\\mjpeg.avi");

			Format format = new Format(EncodingKey, ENCODING_AVI_MJPG, DepthKey, 24, QualityKey, 1f);
	        // Make the format more specific
	        format = format.prepend(MediaTypeKey, MediaType.VIDEO, //
	                FrameRateKey, new Rational(5, 1),//
	                WidthKey, 640, //
	                HeightKey, 480);
			
			//writer del avi
	        AVIWriter out = new AVIWriter(aviSalida);
            // Add a track to the writer
            out.addTrack(format);

			File[] imagenes = new File("c:\\capturado\\").listFiles();
			for(File imagen : imagenes) {
				
				BufferedImage img = ImageIO.read(imagen);
		        Graphics2D g = img.createGraphics();
		        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		        
		        try {

	                // write it to the writer
	                out.write(0, img, 1);

		        } 
		        finally {
		        	
		            // Dispose the graphics object
		            g.dispose();
		        }
			}
			
            // Close the writer
            if (out != null) {
                out.close();
            }
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}