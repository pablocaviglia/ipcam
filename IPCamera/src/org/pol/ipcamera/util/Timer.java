package org.pol.ipcamera.util;

import org.pol.ipcamera.util.listener.TimerListener;

public class Timer extends Thread {
	
	private int segundos;
	private TimerListener timerListener;
	
	public Timer(TimerListener timerListener) {
		this.timerListener = timerListener;
		start();
	}
	
	public void iniciar(int segundos) {
		this.segundos = segundos;
	}
	
	public int getSegundos() {
		return segundos;
	}

	@Override
	public void run() {
		
		while(true) {
			
			if(segundos > 0) {
				--segundos;
				if(segundos == 0) {
					if(timerListener != null) {
						timerListener.timerFinalizado();
					}
				}
			}
			
			try {
				Thread.sleep(1000);
			}
			catch (InterruptedException e) {
				e.printStackTrace();
				FileLogger.logError("Timer - ERR0: " + e.getMessage());
			}
		}
	}
}