package org.pol.ipcamera.config;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class ConfigReader {

	private static Map<String, String> schedule;
	
	public static String SYSTEM_FOLDER;
	public static final String CONFIG_FILE = "system.cfg";

	private static HashMap<String, String> configValues = new HashMap<String, String>();
	
	public static void init(String systemFolder) {
		
		try {
			SYSTEM_FOLDER = systemFolder;
			BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(new File(SYSTEM_FOLDER, CONFIG_FILE))));
			String line = null;
			try {
				while((line=reader.readLine()) != null) {
					if(!line.startsWith("#") && !line.trim().equals("")) {
						String[] splitted = line.split("=");
						String key = splitted[0].trim();
						String value = splitted[1].trim();
						configValues.put(key, value);
					}
				}
			} 
			catch (IOException e) {
				e.printStackTrace();
				System.exit(0);
			}
		} 
		catch (FileNotFoundException e) {
			System.out.println("--------------------------------- ERROR ---------------------------------");
			System.out.println("No se puede acceder al archivo de configuracion.");
			System.out.println("El mismo debe de estar localizado en:");
			System.out.println(SYSTEM_FOLDER + CONFIG_FILE);
			System.out.println("-------------------------------------------------------------------------");
			System.exit(0);
		}
	}
	
	public static String obtenerSegundosGrabacion() {
		return configValues.get("segundos_grabacion");
	}

	public static String obtenerNombreCliente() {
		return configValues.get("nombre_cliente");
	}

	public static String obtenerNombreCamara() {
		return configValues.get("nombre_camara");
	}

	public static String obtenerCameraIP() {
		return configValues.get("camera_ip");
	}
	
	public static String obtenerCameraUser() {
		return configValues.get("camera_user");
	}
	
	public static String obtenerCameraPass() {
		return configValues.get("camera_pass");
	}
	
	public static String obtenerCameraMotionSensitivity() {
		return configValues.get("camera_motion_sensitivity");
	}
	
	public static String obtenerCameraMotionServerIP() {
		return configValues.get("camera_motion_server_ip");
	}
	
	public static String obtenerCameraMotionServerPort() {
		return configValues.get("camera_motion_server_port");
	}
	
	public static String obtenerAWSS3SecretKey() {
		return configValues.get("aws_s3_secret_key");
	}
	
	public static String obtenerAWSS3AccessKeyId() {
		return configValues.get("aws_s3_access_key_id");
	}
	
	public static String obtenerAWSS3BucketName() {
		return configValues.get("aws_s3_bucket_name");
	}
	
	public static String obtenerMotionScheduleStatus() {
		return configValues.get("schedule_enabled");
	}
	
	public static String obtenerZonaHoraria() {
		return configValues.get("zona_horaria");
	}
	
	public static String obtenerCameraFPS() {
		return configValues.get("camera_fps");
	}

	public static String obtenerTamanoMaximoVideo() {
		return configValues.get("tamano_maximo_video");
	}

	public static String obtenerLogErrorFile() {
		return configValues.get("log_error_file");
	}

	public static String obtenerLogFile() {
		return configValues.get("log_file");
	}

	public static Map<String, String> obtenerMotionSchedule() {
		
		//la agenda no fue leida todavia
		//comenzamos la lectura de la misma
		if(schedule == null) {
			schedule = new HashMap<String, String>();
			
			//iterador de claves para obtener solo 
			//aquellas correspondientes al scheduer
			Iterator<String> keys = configValues.keySet().iterator();
			
			while(keys.hasNext()) {
				
				//obtenemos la clave actual
				String currentKey = keys.next();
				String currentKeyTrimmed = currentKey.trim();
				
				//verificamos que corresponda
				//al scheduler
				if( currentKeyTrimmed.startsWith("schedule_sun_") || 
					currentKeyTrimmed.startsWith("schedule_mon_") || 
					currentKeyTrimmed.startsWith("schedule_tue_") ||
					currentKeyTrimmed.startsWith("schedule_wed_") ||
					currentKeyTrimmed.startsWith("schedule_thu_") ||
					currentKeyTrimmed.startsWith("schedule_fri_") ||
					currentKeyTrimmed.startsWith("schedule_sat_")) {
					
					schedule.put(currentKey, configValues.get(currentKey));
				}
			}
		}
		
		return schedule;
	}
}