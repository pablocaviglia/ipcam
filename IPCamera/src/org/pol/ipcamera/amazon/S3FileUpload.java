package org.pol.ipcamera.amazon;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSystemManager;
import org.apache.commons.vfs2.VFS;
import org.pol.ipcamera.amazon.listener.S3Listener;
import org.pol.ipcamera.config.ConfigReader;
import org.pol.ipcamera.util.FileLogger;
import org.pol.ipcamera.util.ProgressInputStream;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;

public class S3FileUpload implements Runnable {
	
	private final AWSCredentials awsCredentials = new AWSCredentials() {
		public String getAWSSecretKey() {return ConfigReader.obtenerAWSS3SecretKey();}
		public String getAWSAccessKeyId() {return ConfigReader.obtenerAWSS3AccessKeyId();}
	};
	
	//listener de eventos
	private S3Listener listener;
	
	//flag que indica si el bucket ya 
	//se encuentra disponible para ser usado
	private boolean bucketDisponible;
	
	public S3FileUpload(S3Listener listener) {
		this.listener = listener;
		new Thread(this).start();
	}
	
	public synchronized void verificarYCrearBucket(String nombre) {
		
		FileLogger.log("(INFO) Verificando existencia de bucket S3 '" + nombre + "'...");
		System.out.println("(INFO) Verificando existencia de bucket S3 '" + nombre + "'...");
		
		//nombre de bucket
		String bucketName = ConfigReader.obtenerAWSS3BucketName();

		// creamos cliente con las credenciales
		// y la region donde subir los archivos
		AmazonS3 s3 = new AmazonS3Client(awsCredentials);
		Region usWest2 = Region.getRegion(Regions.US_WEST_2);
		s3.setRegion(usWest2);

		//verificamos la existencia del bucket
		Bucket bucket = obtenerBucket(s3, bucketName);

		// si no existe lo creamos
		if (bucket == null) {
			try {
				FileLogger.log("(INFO) Creando bucket S3 '" + bucketName + "'");
				System.out.println("(INFO) Creando bucket S3 '" + bucketName + "'");
				s3.createBucket(bucketName);
				bucketDisponible = true;
			} 
			catch (AmazonS3Exception e) {
				if(e.getStatusCode() == 409) {
					FileLogger.log("(ERR-004) Error creando bucket S3, el nombre '" + bucketName + "' ya existe. Los nombres de buckets son compartidos entre todos los usuarios del sistema");
					System.out.println("(ERR-004) Error creando bucket S3, el nombre '" + bucketName + "' ya existe. Los nombres de buckets son compartidos entre todos los usuarios del sistema");
				}
				else {
					FileLogger.log("(ERR-005) Error inesperado S3: " + e.getMessage());
					System.out.println("(ERR-005) Error inesperado S3: " + e.getMessage());
				}
			}
		}
		else {
			bucketDisponible = true;
		}
	}
	
	public synchronized void crearCarpeta(String nombre) {
		
		// creamos cliente con las credenciales
		// y la region donde subir los archivos
		AmazonS3 s3 = new AmazonS3Client(awsCredentials);
		Region usWest2 = Region.getRegion(Regions.US_WEST_2);
		s3.setRegion(usWest2);

		//nombre de bucket
		String bucketName = ConfigReader.obtenerAWSS3BucketName();
		
		//create metadata for your folder & set content-length to 0
		ObjectMetadata metadata = new ObjectMetadata();
		metadata.setContentLength(0);

		//create empty content
		InputStream emptyContent = new ByteArrayInputStream(new byte[0]);
		
		// Create a PutObjectRequest passing the foldername suffixed by /
		PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, nombre + "/", emptyContent, metadata);
		 
		// Send request to S3 to create folder
		s3.putObject(putObjectRequest);

	}
	
	/**
	 * Encola un archivo para ser subido por 
	 * el thread encargado de esto
	 * @param file
	 * @param serverFilename
	 */
	public synchronized void encolarSubidaArchivo(File file, String serverFilename, long duracion) {
		
		try {
			
			//creamos el contenido del archivo
			//que define la subida del archivo
			//a colocar en la cola de subida
			String contenido = file.getAbsolutePath() + "," + serverFilename + "," + duracion;
			
			//verificamos la existencia de la
			//carpeta y su creacion si es necesario
			File carpetaArchivosCola = new File(ConfigReader.SYSTEM_FOLDER, "upload_queue");
			carpetaArchivosCola.mkdirs();
			
			//referenciamos el archivo para encolar
			//un nuevo archivo para subir
			File archivoCola = new File(carpetaArchivosCola, String.valueOf(new Date().getTime()));
			BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(archivoCola));
			bos.write(contenido.getBytes());
			bos.flush();
			bos.close();
			
		} 
		catch (Exception e) {
			FileLogger.logError("S3FileUpload - ERR3: " + e.getMessage());
		}
	}
	
	/**
	 * Obtiene la lista de archivos encolados
	 * @return
	 */
	public synchronized List<File> obtenerArchivosEncolados() {
		
		List<File> archivosEncolados = new ArrayList<File>();
		
		//referenciamos la carpeta que contiene
		//los archivos encolados para subir
		File carpetaArchivosCola = new File(ConfigReader.SYSTEM_FOLDER, "upload_queue");
		
		//obtenemos el array de archivos
		//que no sean videos asf
		File[] filesArray = carpetaArchivosCola.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				if(name.trim().endsWith("asf")) {
					return false;
				}
				else {
					return true;
				}
			}
		});
		
		if(filesArray != null && filesArray.length > 0) {
			
			//ordenamos la lista de archivos lexicograficamente
			//basandonos en el nombre de archivo
			Arrays.sort(filesArray);
			
			//transformamos el array en una lista
			archivosEncolados = Arrays.asList(filesArray);
		}
		
		return archivosEncolados;
	}
	
	private synchronized boolean subirArchivo(File file, String serverFilename) {
		
		if(file.exists()) {
			
			try {

				// nombre de archivos local
				String localFilename = file.getAbsolutePath();
				FileSystemManager fsManager = VFS.getManager();
				FileObject fileObject = fsManager.resolveFile(localFilename);

				//nombre de bucket
				String bucketName = ConfigReader.obtenerAWSS3BucketName();
	
				// creamos cliente con las credenciales
				// y la region donde subir los archivos
				AmazonS3 s3 = new AmazonS3Client(awsCredentials);
				Region usWest2 = Region.getRegion(Regions.US_WEST_2);
				s3.setRegion(usWest2);
	
				// metadata que indica el tamano
				// del archivo siendo subido
				ObjectMetadata objectMetadata = new ObjectMetadata();
				objectMetadata.setContentLength(fileObject.getContent().getSize());
	
				FileLogger.log("(INFO) Creando nuevo archivo S3 '" + localFilename + "'");
				System.out.println("(INFO) Creando nuevo archivo S3 '" + localFilename + "'");
				
				s3.putObject(new PutObjectRequest(bucketName, serverFilename, new ProgressInputStream(file.getName(), fileObject.getContent()), objectMetadata));

				//cerramos recursos
				fileObject.close();

			} 
			catch (Exception e) {

				System.out.println("(ERR-007) Hubo un error mientras se intentaba subir el archivo '" + file.getAbsolutePath() +"' a S3: " + e.getMessage());
				FileLogger.logError("S3FileUpload - ERR2: " + e.getMessage());
				
				return false;
			}

			//indicamos que archivo fue subido correctamente
			return true;
		}
		else {
			FileLogger.log("(ERR-006) El archivo marcado para subir no existe: " + file.getAbsolutePath());
			System.out.println("(ERR-006) El archivo marcado para subir no existe: " + file.getAbsolutePath());
			return false;
		}
	}
	
	public synchronized void subirData(byte[] data, String serverFilename) {
		
		try {
		
			//nombre de bucket
			String bucketName = ConfigReader.obtenerAWSS3BucketName();

			// creamos cliente con las credenciales
			// y la region donde subir los archivos
			AmazonS3 s3 = new AmazonS3Client(awsCredentials);
			Region usWest2 = Region.getRegion(Regions.US_WEST_2);
			s3.setRegion(usWest2);
			
			// metadata que indica el tamano
			// del archivo siendo subido
			ObjectMetadata objectMetadata = new ObjectMetadata();
			objectMetadata.setContentLength(data.length);
			
			FileLogger.log("(INFO) Creando nuevo archivo S3 '" + serverFilename + "'");
			System.out.println("(INFO) Creando nuevo archivo S3 '" + serverFilename + "'");
			s3.putObject(new PutObjectRequest(bucketName, serverFilename, new ByteArrayInputStream(data), objectMetadata));

		} 
		catch (Exception e) {
			FileLogger.logError("S3FileUpload - ERR1: " + e.getMessage());
		}
	}
	
	private synchronized Bucket obtenerBucket(AmazonS3 s3, String bucketName) {
		
		List<Bucket> buckets = s3.listBuckets();
		for(Bucket bucket : buckets) {
			if(bucket.getName().equals(bucketName)) {
				return bucket;
			}
		}
		
		return null;
	}

	public void run() {
		
		while (true) {

			try {

				if (bucketDisponible) {
					
					// obtenemos la lista de archivos
					// prontos para ser subidos
					List<File> archivosEncolados = obtenerArchivosEncolados();
					
					// recorremos todos los archivos
					for (File archivoEncolado : archivosEncolados) {
						
						// leemos el contenido del archivo encolado
						// que posee el path del archivo a subir, y
						// el nombre que va a ser usado en S3 para
						// referencia ese archivo
						BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(archivoEncolado)));

						// obtenemos el contenido
						String contenidoArchivoEncolado = reader.readLine();
						String[] contenidoSplitted = contenidoArchivoEncolado.split(",");

						// cerramos recursos
						reader.close();
						
						// el valor previo a la coma es el path
						// al archivo a ser subido
						String pathArchivoSubir = contenidoSplitted[0].trim();
						File archivoSubir = new File(pathArchivoSubir);
						String nombreArchivoServer = contenidoSplitted[1].trim();
						String duracion = contenidoSplitted[2].trim();
						
						// subimos el archivo
						boolean archivoSubido = subirArchivo(archivoSubir, nombreArchivoServer.replaceAll("@", duracion));
						
						// llamamos al listener que indica
						// que el archivo termino de subirse
						if (archivoSubido) {
							
							//llamamos al listener post subida
							listener.fileUploadFinished(archivoSubir);

							// si llegamos a esta linea es debido a que
							// el archivo pudo subirse correctamente por
							// lo tanto lo borramos del encolado de archivos
							archivoEncolado.delete();

						}
						else {
							
							//si el archivo no fue subido porque
							//el mismo no existe, entonces borramos
							//el archivo de encolado
							if(!archivoSubir.exists()) {
								archivoEncolado.delete();
							}
						}
					}
				}

				// dormimos el hilo un
				// periodo determinado
				Thread.sleep(5000);

			} 
			catch (Exception e) {
				FileLogger.logError("S3FileUpload - ERR0: " + e.getMessage());
			}
		}
	}
}