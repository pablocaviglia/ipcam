package org.pol.ipcamera.amazon.listener;

import java.io.File;

public interface S3Listener {

	public void fileUploadFinished(File file);
	
}
