package org.pol.ipcamera.stream;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URL;

public class CameraStreamer {

	public static void main(String[] args) {
		
		while(true) {
			
			InputStream is = null;
			OutputStream os = null;
			ServerSocket serverSocket = null;
			
			try {
				
				serverSocket = new ServerSocket(80);
				
				//espera hasta recibir una conexion
				Socket clientSocket = serverSocket.accept();
				
				System.out.println("Conexion establecida con cliente: " + clientSocket);
				
				//creamos el flujo de entrada 
				//desde la camara
				URL url = new URL("http://192.168.1.102/videostream.cgi?user=admin&pwd=&resolution=32&rate=11");
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				System.out.println("Conexion establecida con camara: " + conn);
				
				//flujo de lectura desde la camara
				//y flujo de escritura hacia el puerto
				//del cliente
				is = conn.getInputStream();
				os = clientSocket.getOutputStream();
				
				int i = 0;
				while((i = is.read()) != -1) {
					os.write(i);
				}
				
			} 
			catch (Exception e) {
				e.printStackTrace();
			}
			finally {
				try {
					if(os != null) {
						os.flush();
						os.close();
					}
					if(is != null) {
						is.close();
					}
					if(serverSocket != null) {
						serverSocket.close();
						serverSocket = null;
					}
				} 
				catch (IOException e) {
					e.printStackTrace();
				}
			}	
		}
	}
}
