package org.pol.ipcamera;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import org.pol.ipcamera.amazon.S3FileUpload;
import org.pol.ipcamera.amazon.listener.S3Listener;
import org.pol.ipcamera.capture.IPCaptureASF;
import org.pol.ipcamera.capture.listener.CaptureListener;
import org.pol.ipcamera.config.ConfigReader;
import org.pol.ipcamera.motion.MotionDetector;
import org.pol.ipcamera.motion.listener.MotionListener;
import org.pol.ipcamera.util.ASFVideoValidator;
import org.pol.ipcamera.util.FileLogger;
import org.pol.ipcamera.util.Timer;
import org.pol.ipcamera.util.listener.TimerListener;
import org.pol.ipcamera.util.listener.VideoReindexerListener;

public class Surveillance implements MotionListener, TimerListener, CaptureListener, VideoReindexerListener, S3Listener {
	
	//lock de hilos
	private Object lock = new Object();
	
	//formateo de fecha
	private SimpleDateFormat s3FolderDateFormat = new SimpleDateFormat("yyyyMMdd");

	//referencia a si mismo
	private Surveillance self;
	
	//deteccion de movimiento
	private MotionDetector motionDetector;
	
	//amazon cloud
	private S3FileUpload s3FileUpload;
	
	//video capture
	private IPCaptureASF ipCaptureASF;
	
	//timer de grabacion
	private Timer timer;
	
	public Surveillance() {

		boolean initialized = false;
		
		while(!initialized) {
			
			try {

				//limpiamos recursos
				limpiarCarpetaUploadQueue();
				
				TimeZone timezone = TimeZone.getTimeZone("GMT" + ConfigReader.obtenerZonaHoraria());
				s3FolderDateFormat.setTimeZone(timezone);
				self = this;
				s3FileUpload = new S3FileUpload(this);

				//creamos el bucket en S3 en
				//caso de que no exista
				s3FileUpload.verificarYCrearBucket(ConfigReader.obtenerAWSS3BucketName());
				
				//se finalizo la inicializacion
				initialized = true;
			} 
			catch (Exception e) {
				
				FileLogger.logError("Initializing Surveillance - ERR1: " + e.getMessage());
				System.out.println("Initializing Surveillance - ERR1: " + e.getMessage());
				
				try {Thread.sleep(10000);} catch (InterruptedException e1) {e1.printStackTrace();}
			}
		}
		
		motionDetector = new MotionDetector(this);
		ipCaptureASF = new IPCaptureASF(this);
		timer = new Timer(this);
		
		//iniciamos la primer grabacion 
		//ni bien comienza el sistema
		comenzarGrabacion();

	}
	
	/**
	 * Limpia la carpeta de upload queue
	 * removiendo los recursos invalidos
	 */
	private void limpiarCarpetaUploadQueue() {
		
		//carpeta y archivos de salida
		File outputFolder = new File(ConfigReader.SYSTEM_FOLDER + "/upload_queue/");
		
		if(outputFolder.exists()) {
			
			//obtenemos los archivos que definen
			//la subida del archivo asf
			File[] uploadQueueFilesArray = outputFolder.listFiles(new FilenameFilter() {
				public boolean accept(File dir, String name) {
					if(!name.trim().endsWith("asf")) {
						return true;
					}
					return false;
				}
			});
			
			//obtenemos los archivos asf
			File[] asfFilesArray = outputFolder.listFiles(new FilenameFilter() {
				public boolean accept(File dir, String name) {
					if(name.trim().endsWith("asf")) {
						return true;
					}
					return false;
				}
			});
			
			//recorremos los archivos ASF
			for(File archivoASFActual : asfFilesArray) {
				
				//flag que indica si existe
				//un queue de subida para el
				//archivo ASF actual
				boolean existeQueueSubidaASF = false;
				
				//recorremos los queue de subidas
				for(File archivoQueueActual : uploadQueueFilesArray) {
					
					try {

						// leemos el contenido del archivo encolado
						// que posee el path del archivo a subir, y
						// el nombre que va a ser usado en S3 para
						// referencia ese archivo
						BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(archivoQueueActual)));

						// obtenemos el contenido
						String contenidoArchivoEncolado = reader.readLine();
						String[] contenidoSplitted = contenidoArchivoEncolado.split(",");
						String pathArchivoSubir = contenidoSplitted[0].trim();
						
						if(archivoASFActual.getAbsolutePath().equals(pathArchivoSubir)) {
							existeQueueSubidaASF = true;
							break;
						}
					} 
					catch (Exception e) {
						e.printStackTrace();
					}
				}
				
				if(!existeQueueSubidaASF) {
					archivoASFActual.delete();
				}
			}
		}
	}
	
	private void comenzarGrabacion() {
		
		//lock entre la deteccion de movimiento para comenzar
		//una nueva grabacion, y la finalizacion del timer para
		//detener la grabacion que puede estar efectuandose
		synchronized (lock) {
			
			//comenzamos el proceso de grabacion del
			//video hacia el sistema de archivos
			ipCaptureASF.grabar();
			
			//configuramos el timer para que grabe
			//por un tiempo determinado
			timer.iniciar(Integer.parseInt(ConfigReader.obtenerSegundosGrabacion()));
			
		}
	}
	
	public void motionDetected() {
		
		//comenzamos la grabacion
		comenzarGrabacion();
	}
	
	public void timerFinalizado() {

		//lock entre la deteccion de movimiento para comenzar
		//una nueva grabacion, y la finalizacion del timer para
		//detener la grabacion que puede estar efectuandose
		synchronized (lock) {
			ipCaptureASF.detener();
		}
	}
	
	public void capturaFinalizada(final File capturaFile, final long fechaInicio, final long fechaFin) {
		
		//ejecutamos en un hilo toda la
		//ejecucion relacionada a la nube
		new Thread(new Runnable() {
			public void run() {
				try {
					
					//verificamos que el video capturado
					//no se encuentre corrupto
					if(ASFVideoValidator.validate(capturaFile)) {
						
						//calculamos la duracion del video
						long duracion = fechaFin > 0 ? fechaFin - fechaInicio : 0;
						
						//definimos el nombre de la carpeta
						//a donde va a ser subido el video
						String nombreCarpetaS3 = ConfigReader.obtenerNombreCliente() + "/" + s3FolderDateFormat.format(new Date(fechaInicio)) + "/" + ConfigReader.obtenerNombreCamara();
						
						//comenzamos a subir el video a la nube
						s3FileUpload.encolarSubidaArchivo(capturaFile, nombreCarpetaS3  + "/" + capturaFile.getName().replaceAll("_WOIndex.asf", "_@_WOIndex.asf"), duracion);
						
					}
					else {
						
						//borramos el archivo 
						//porque esta corrupto}
						capturaFile.delete();
					}
				}
				catch (Exception e) {
					FileLogger.logError("Surveillance - ERR0: " + e.getMessage());
				}
			}
		}).start();
	}
	
	public void videoReindexed(File input, File output) {
		
		//solo si existe archivo de salida, y el tamano
		//de archivo del mismo es mayor que el de entrada
		//(debido a que hay indice creado) se borra el 
		//archivo original
		if(output != null && output.exists() && input.length() < output.length()) {
		
			System.out.println("VIDEO INDEXING FINISHED!");
			
			//borramos el video original
			input.delete();
			
		}
	}

	public void fileUploadFinished(File uploadedFile) {
		
		FileLogger.log("(INFO) Subida de archivo finalizada '" + uploadedFile.getAbsolutePath() + "'");
		System.out.println("(INFO) Subida de archivo finalizada '" + uploadedFile.getAbsolutePath() + "'");

		//borramos el archivo local una 
		//vez que el mismo fue subido a
		//la nube
		uploadedFile.delete();
		
	}

	public static void main(String[] args) {
		
		try {

			if (args != null && args.length > 0) {
				ConfigReader.init(args[0]);
				new Surveillance();
			} 
			else {
				System.out.println("--------------------------------- ERROR ---------------------------------");
				System.out.println("Debe de especificar la carpeta donde se encuentra el archivo");
				System.out.println("de configuracion como parametro de este programa.");
				System.out.println("-------------------------------------------------------------------------");
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}