package org.pol.ipcamera.motion;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;
import java.util.Map;

import org.pol.ipcamera.config.ConfigReader;

public class MotionScheduler {
	
	/**
	 * Configura el schedule del sensor 
	 * de movimiento de  la camara
	 * @throws IOException 
	 */
	public static void configurarMotionSchedule() throws IOException {
		
		String motionScheduleEnabled = ConfigReader.obtenerMotionScheduleStatus();
		if(motionScheduleEnabled != null && motionScheduleEnabled.trim().toLowerCase().equals("true")) {
			
			//obtenemos el schedule
			Map<String, String> motionSchedule = ConfigReader.obtenerMotionSchedule();
			
			//obtenemos las claves del schedule
			Iterator<String> keys = motionSchedule.keySet().iterator();
			
			//recorremos las claves y creamos grupos 
			//de 10 configuraciones las cuales enviamos
			//como un request a la camara
			while(keys.hasNext()) {
				
				//obtenemos la clave y valor actual
				String currentKey = keys.next();
				String currentValue = motionSchedule.get(currentKey);
				
				//creamos dinamicamente el request
				//peticion request creada dinamicamente
				//con una clave y un valor
				String requestDinamico = "&schedule_sun_0=16777216";
				
				URL url = new URL("http://" + ConfigReader.obtenerCameraIP() + "/set_alarm.cgi?user=" + ConfigReader.obtenerCameraUser() + "&pwd=" + ConfigReader.obtenerCameraPass() + "&schedule_enable=1" + requestDinamico);
				System.out.println(url);
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();

				BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
				String inputLine;
				while ((inputLine = in.readLine()) != null) {
					if (inputLine.equals("ok.")) {
						System.out.println("SCHEDULE SET (" + currentKey + "=" + currentValue + ")");
						break;
					}
				}
				
				//cerramos recursos
				in.close();
				conn.disconnect();
				
			}
		}
	}
	
	public static void main(String[] args) {
		
		try {
			
			MotionScheduler.configurarMotionSchedule();
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}