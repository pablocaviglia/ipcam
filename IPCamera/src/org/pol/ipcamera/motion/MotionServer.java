package org.pol.ipcamera.motion;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

import org.pol.ipcamera.motion.listener.MotionListener;
import org.pol.ipcamera.util.FileLogger;

public class MotionServer extends Thread {

	private ServerSocket serverSocket;
	private String ip;
	private int port;
	private MotionListener motionListener;

	public MotionServer(String ip, int port, MotionListener motionListener) {
		this.motionListener = motionListener;
		this.ip = ip;
		this.port = port;
		start();
	}

	@Override
	public void run() {

		// el server siempre escucha
		// luego de aceptar una peticion
		while (true) {

			try {

				if (serverSocket == null || serverSocket.isClosed()) {
					InetAddress addr = InetAddress.getByName(ip);
					serverSocket = new ServerSocket(port, 0, addr);
				}

				// espera hasta recibir una conexion
				final Socket clientSocket = serverSocket.accept();

				try {
					BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
					String inputLine = null;
					while ((inputLine = in.readLine()) != null) {
						System.out.println("(INFO) Movimiento detectado");
						FileLogger.log("(INFO) Movimiento detectado");
						// si hay un listener le avisamos que se
						// detecto un evento de movimiento
						if (motionListener != null) {
							motionListener.motionDetected();
						}
						break;
					}
					in.close();
				} 
				catch (Exception e) {
					e.printStackTrace();
				}
				
				//cerramos recursos
				clientSocket.close();
				serverSocket.close();

			} 
			catch (Exception e) {
				System.out.println("(ERR-001) No puede crearse el servidor de deteccion de movimiento: " + e.getMessage());
				FileLogger.logError("MotionServer - ERR0: " + e.getMessage());
			}
		}
	}
}