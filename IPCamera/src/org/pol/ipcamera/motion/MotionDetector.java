package org.pol.ipcamera.motion;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.pol.ipcamera.config.ConfigReader;
import org.pol.ipcamera.motion.listener.MotionListener;
import org.pol.ipcamera.util.FileLogger;

public class MotionDetector {

	//server escuchando por eventos de
	//movimiento generados por la camara
	private MotionServer motionServer;
	
	//listener al cual avisar cuando
	//un evento de movimiento es detectado
	private MotionListener motionListener;
	
	public MotionDetector(MotionListener motionListener) {
		
		//referenciamos
		this.motionListener = motionListener;
		
		//crea el servidor que acepta
		//peticiones de la camara
		crearServidor();
		
		try {
			//habilita la alarma de movimiento
			habilitarAlarmaMovimiento();
		} 
		catch (Exception e) {
			System.out.println("(ERR-002) No puede activarse el sensor de movimiento en la camara: " + e.getMessage());
			FileLogger.logError("MotionDetector - ERR0: " + e.getMessage());

		}
	}
	
	/**
	 * Crea el servidor que escucha
     * los eventos de movimiento generados
     * por la camara
	 */
	private void crearServidor() {
		motionServer = new MotionServer(ConfigReader.obtenerCameraMotionServerIP(), Integer.parseInt(ConfigReader.obtenerCameraMotionServerPort()), motionListener);
	}
	
	/**
	 * Habilita la alarma de movimento
	 * ejecutando el comando cgi en la camara
	 */
	private void habilitarAlarmaMovimiento() throws IOException {
		
		String alarmVisitUrl = "http://" + ConfigReader.obtenerCameraMotionServerIP() + ":" + ConfigReader.obtenerCameraMotionServerPort();
		URL url = new URL("http://" + ConfigReader.obtenerCameraIP() + "/set_alarm.cgi?user=" + ConfigReader.obtenerCameraUser() + "&pwd=" + ConfigReader.obtenerCameraPass() + "&motion_armed=1&ioin_level=1&ioout_level=1&motion_sensitivity=" + ConfigReader.obtenerCameraMotionSensitivity() + "&http=1&http_url=" + alarmVisitUrl);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();

		BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		String inputLine;
		while ((inputLine = in.readLine()) != null) {
			if (inputLine.equals("ok.")) {
				FileLogger.log("(INFO) Sistema de deteccion de movimiento activado");
				System.out.println("(INFO) Sistema de deteccion de movimiento activado");
				break;
			}
		}
		
		//cerramos recursos
		in.close();
		conn.disconnect();
	}
}