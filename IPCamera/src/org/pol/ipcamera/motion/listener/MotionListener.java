package org.pol.ipcamera.motion.listener;

public interface MotionListener {

	/**
	 * Un evento de movimiento fue detectado por la camara 
	 */
	public void motionDetected();
	
}
