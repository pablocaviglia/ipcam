package org.pol.ipcamera.capture;


import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.pol.ipcamera.util.Base64Encoder;

public class IPCaptureMJPEG implements Runnable {

	private String CAPTURE_FOLDER = "c:\\capturado\\";
	private String CAPTURE_VIDEO_ONLY_URL = "http://192.168.0.101/videostream.cgi?rate=11";
	private String CAPTURE_VIDEO_ONLY_USER = "admin";
	private String CAPTURE_VIDEO_ONLY_PASS = "";

	private boolean listenerAvailable;
	private byte[] curFrame;
	private boolean frameStarted;
	private boolean frameAvailable;
	private Thread streamReader;
	private HttpURLConnection conn;
	private BufferedInputStream httpIn;
	private ByteArrayOutputStream jpgOut;
	private volatile boolean keepAlive;

	public IPCaptureMJPEG() {

		super();
		this.curFrame = new byte[0];
		this.frameStarted = false;
		this.frameAvailable = false;
		this.keepAlive = false;
		
		try {
			//creamos la carpeta de captura
			new File(CAPTURE_FOLDER).mkdirs();
			start();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	public boolean isAlive() {
		return streamReader.isAlive();
	}

	public boolean isAvailable() {
		return frameAvailable;
	}

	public void start() {
		if (streamReader != null && streamReader.isAlive()) {
			System.out.println("Camera already started");
			return;
		}
		streamReader = new Thread(this, "HTTP Stream reader");
		keepAlive = true;
		streamReader.start();
	}

	public void stop() {
		if (streamReader == null || !streamReader.isAlive()) {
			System.out.println("Camera already stopped");
			return;
		}
		keepAlive = false;
		try {
			streamReader.join();
		} catch (InterruptedException e) {
			System.err.println(e.getMessage());
		}
	}

	public void dispose() {
		stop();
	}

	public void run() {
		
		URL url;
		Base64Encoder base64 = new Base64Encoder();

		try {
			url = new URL(CAPTURE_VIDEO_ONLY_URL);
		} catch (MalformedURLException e) {
			System.err.println("Invalid URL");
			return;
		}

		try {
			conn = (HttpURLConnection) url.openConnection();
			conn.setRequestProperty("Authorization", "Basic " + base64.encode(CAPTURE_VIDEO_ONLY_USER + ":" + CAPTURE_VIDEO_ONLY_PASS));
		} catch (IOException e) {
			System.err.println("Unable to connect: " + e.getMessage());
			return;
		}
		try {
			httpIn = new BufferedInputStream(conn.getInputStream(), 8192);
			jpgOut = new ByteArrayOutputStream(8192);
		} catch (IOException e) {
			System.err.println("Unable to open I/O streams: " + e.getMessage());
			return;
		}

		int prev = 0;
		int cur = 0;

		try {
			while (keepAlive && (cur = httpIn.read()) >= 0) {
				if (prev == 0xFF && cur == 0xD8) {
					frameStarted = true;
					jpgOut.close();
					jpgOut = new ByteArrayOutputStream(8192);
					jpgOut.write((byte) prev);
				}
				if (frameStarted) {
					jpgOut.write((byte) cur);
					if (prev == 0xFF && cur == 0xD9) {
						curFrame = jpgOut.toByteArray();
						frameStarted = false;
						frameAvailable = true;
						
						//si existe un frame disponible y 
						//hay un listener asociado, lo llamamos
						if(listenerAvailable) {
							frameAvailable(curFrame);
							frameAvailable = false;
						}
					}
				}
				prev = cur;
			}
		} 
		catch (IOException e) {
			System.err.println("I/O Error: " + e.getMessage());
		}
		try {
			jpgOut.close();
			httpIn.close();
		} catch (IOException e) {
			System.err.println("Error closing I/O streams: " + e.getMessage());
		}
		conn.disconnect();
	}
	
	public void frameAvailable(byte[] frame) {
		
		try {
			File f = new File(CAPTURE_FOLDER + System.nanoTime() + ".jpg");
			f.createNewFile();
			FileOutputStream fos = new FileOutputStream(f);
			fos.write(frame);
			fos.flush();
			fos.close();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		new IPCaptureMJPEG();
	}
}