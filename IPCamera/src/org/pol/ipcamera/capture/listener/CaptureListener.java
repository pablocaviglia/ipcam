package org.pol.ipcamera.capture.listener;

import java.io.File;

public interface CaptureListener {

	public void capturaFinalizada(File capturaFile, long fechaInicio, long fechaFin);
	
}
