package org.pol.ipcamera.capture;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import org.pol.ipcamera.capture.listener.CaptureListener;
import org.pol.ipcamera.config.ConfigReader;
import org.pol.ipcamera.util.FileLogger;

public class IPCaptureASF {

	//formato de fecha 
	private SimpleDateFormat videoFileDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
	
	//flag usado mientras se esta 
	//deteniendo la grabacion
	private boolean deteniendo;
	
	//flag usado para indicar que se
	//encuentra grabando actualmente
	private boolean grabando;
	
	//listener de eventos de captura
	private CaptureListener listener;
	
	//flujo de entrada y salida para 
	//leer desde la camara y escribir
	//al sistema de archivos
	public FileOutputStream fos = null;
	public BufferedInputStream bis = null;
	
	//leemos el tamano maximo de los videos
	public final static long TAMANO_MAXIMO_VIDEOS = Long.valueOf(ConfigReader.obtenerTamanoMaximoVideo().trim()) * 1024 * 1024;
	
	
	
	public IPCaptureASF(CaptureListener listener) {

		TimeZone timezone = TimeZone.getTimeZone("GMT" + ConfigReader.obtenerZonaHoraria());
		videoFileDateFormat.setTimeZone(timezone);
		this.listener = listener;
		
		//iniciamos hilo que vigila por
		//errores durante la grabacion
		new CapturadorWatcher().start();
		
		//iniciamos hilo de grabacion
		new Capturador().start();

	}
	
	public synchronized void grabar() {
		grabando = true;
	}
	
	public synchronized void detener() {
		deteniendo = true;
	}
	
	class CapturadorWatcher extends Thread {
		
		//tiempo que no hubo consumo
		//desde la camara
		private long tiempoCamaraSinConsumo;
		
		//tmp var que guarda la ultima
		//cantidad de bytes leidos
		private long cantidadBytesLeidosTmp;
		private final long SLEEP_TIME = 2000;
		
		public void run() {
			
			while(true) {

				if(fos != null && fos.getChannel().isOpen()) {
					
					long fileSize = 0;
					try {
						fileSize = fos.getChannel().size();
					} 
					catch (Exception e) {
						FileLogger.logError("(ERR-005) Error en CapturadorWatcher");
						System.out.println("(ERR-005) Error en CapturadorWatcher");
					}
						
					if(cantidadBytesLeidosTmp == fileSize) {
						tiempoCamaraSinConsumo += SLEEP_TIME;
					}
					else {
						tiempoCamaraSinConsumo = 0;
					}
					
					//actualizamos
					cantidadBytesLeidosTmp = fileSize;

					//si el tiempo sin consumo desde la camara
					//es mayor a determinado tiempo detenemos
					//la grabacion
					if(tiempoCamaraSinConsumo > 10000) {
						try {
							bis.close();
							fos.close();
						} 
						catch (IOException e) {
							FileLogger.logError("(ERR-004) Timeout esperando datos desde la camara");
							System.out.println("(ERR-004) Timeout esperando datos desde la camara");
						}
					}
				}
				
				try {
					//dormimos unos segundos
					Thread.sleep(SLEEP_TIME);
				} 
				catch (InterruptedException e) {
				}
			}
		}
	}
	
	class Capturador extends Thread {

		private long bytesLeidos;
		
		public void run() {
			
			while(true) {
				
				if(grabando) {
					
					FileLogger.log("(INFO) Comenzando grabacion desde camara");
					System.out.println("(INFO) Comenzando grabacion desde camara");

					//resetamos valores de 
					//inicio y fin
					long fechaInicio = 0;
					long fechaFin = 0;
					
					//carpeta y archivos de salida
					File outputFolder = new File(ConfigReader.SYSTEM_FOLDER + "/upload_queue/");
					File outputFile = new File(outputFolder, videoFileDateFormat.format(new Date(System.currentTimeMillis())) + "_WOIndex.asf");
					
					//creamos la carpeta si no existe
					outputFolder.mkdirs();
					
					try {
						
						//flujo de lectura desde la camara
						URL url = new URL("http://" + ConfigReader.obtenerCameraIP() + "/videostream.asf?user=" + ConfigReader.obtenerCameraUser() + "&pwd=" + ConfigReader.obtenerCameraPass() + "&resolution=32&rate=" + ConfigReader.obtenerCameraFPS());
						HttpURLConnection conn = (HttpURLConnection) url.openConnection();
						bis = new BufferedInputStream(conn.getInputStream());
						
						//flujo de escritura hacia archivo
						fos = new FileOutputStream(outputFile);
						
						//indica la cantidad total de bytes
						//leidos desde la camara durante la
						//grabacion actual
						bytesLeidos = 0l;
						
						//obtenemos la fecha de inicio
						//de la grabacion
						fechaInicio = System.currentTimeMillis();
						
						byte[] buffer = new byte[32768];
						int i = 0;
						while((i = bis.read(buffer)) != -1) {
							
							//incrementamos contador de bytes
							bytesLeidos += i;
							
							//escribimos bytes al output
							fos.write(buffer, 0, i);
							
							if(bytesLeidos >= TAMANO_MAXIMO_VIDEOS || deteniendo) {
								
								FileLogger.log("(INFO) Grabacion desde camara finalizada!");
								System.out.println("(INFO) Grabacion desde camara finalizada!");
								
								//cerramos recursos
								fos.flush();
								fos.close();
								
								//cambiamos flags
								grabando = false;
								deteniendo = false;
								
								//obtenemos la fecha de fin
								//de la grabacion
								fechaFin = System.currentTimeMillis();
								
								//llamamos al listener
								listener.capturaFinalizada(outputFile, fechaInicio, fechaFin);
								
								if(bytesLeidos >= TAMANO_MAXIMO_VIDEOS) {
									grabando = true;
								}
								
								//finalizamos el while de 
								//lectura desde la camara
								break;
							}
						}
					}
					catch (Exception e) {
						
						FileLogger.logError("(ERR-003) No puede grabarse video desde la camara: " + e.getMessage());
						System.out.println("(ERR-003) No puede grabarse video desde la camara: " + e.getMessage());
						
						//cambiamos flags
						grabando = false;
						deteniendo = false;
						
						if(bytesLeidos >= TAMANO_MAXIMO_VIDEOS) {
							grabando = true;
						}
						
						//llamamos al listener
						listener.capturaFinalizada(outputFile, fechaInicio, 0);
						
					} 
					finally {
						//cerramos flujos
						try {
							if(fos != null) {
								fos.flush();
								fos.close();
							}
							if(bis != null) {
								bis.close();
							}
						} 
						catch (Exception e) {
							FileLogger.logError("IPCaptureASF - ERR2: " + e.getMessage());
						}
					}
				}
				
				try {
					Thread.sleep(500);
				} 
				catch (Exception e) {
				}
			}
		}
	}
}