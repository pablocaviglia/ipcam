package org.pol.ipcamera.servo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.pol.ipcamera.config.ConfigReader;

public class ServoControl {

	public static final byte MOVE_UP = 0;
	public static final byte MOVE_STOP_UP = 1;
	public static final byte MOVE_DOWN = 2;
	public static final byte MOVE_STOP_DOWN = 3;
	public static final byte MOVE_LEFT = 4;
	public static final byte MOVE_STOP_LEFT = 5;
	public static final byte MOVE_RIGHT = 6;
	public static final byte MOVE_STOP_RIGHT = 7;
	
	public static void moverServo(byte movimiento, int grados) throws IOException {
		
		//creamos el flujo de entrada 
		//desde la camara
		URL url = new URL("http://" + ConfigReader.obtenerCameraIP() + "/decoder_control.cgi?command=" + movimiento  + "&onestep=1&degree=" + grados + "&user=" + ConfigReader.obtenerCameraUser() + "&pwd=" + ConfigReader.obtenerCameraPass());
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		
		BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		String inputLine;
		while ((inputLine = in.readLine()) != null) {
			if (inputLine.equals("ok.")) {
				System.out.println("SERVO SUCCESSFULLY MOVED");
				break;
			}
		}
		
		//cerramos recursos
		in.close();
		conn.disconnect();

	}
	
	public static void main(String[] args) {
		
		try {
			ConfigReader.init("/home/pi/ipcamera/");
			ServoControl.moverServo(MOVE_DOWN, 10);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}